# n16r_shell

shell scripts to generate numeronyms
such as proposed in the following article [on stackexchange](https://unix.stackexchange.com/questions/548540/how-to-create-numeronyms-in-bash)

an example command would be
```
awk '{l=length($1); print substr($1,1,1) l-2 substr($1,l,1)}'
```